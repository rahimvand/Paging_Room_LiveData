package hamed.pagingroomlivedata;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName =  "course")
public class Course {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;

    public Course() {
    }

    public Course(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}