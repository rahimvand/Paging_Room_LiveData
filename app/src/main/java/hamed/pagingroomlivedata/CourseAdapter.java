package hamed.pagingroomlivedata;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

public class CourseAdapter extends PagedListAdapter<Course, CourseAdapter.CourseViewHolder> {
    private Context context;

    CourseAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.context = context;
    }

    @NonNull
    @Override
    public CourseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_course, parent, false);
        return new CourseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CourseViewHolder holder, int position) {

        Course course = getItem(position);
        if (course != null) {
            holder.bind(course);
        } else {
            holder.clear();
        }
    }

    class CourseViewHolder extends RecyclerView.ViewHolder {

        TextView txvId, txvName;

        CourseViewHolder(@NonNull View itemView) {
            super(itemView);
            txvId = itemView.findViewById(R.id.txv_courseId);
            txvName = itemView.findViewById(R.id.txv_courseName);
        }

        void bind(Course course) {
            txvId.setText(String.valueOf(course.getId()));
            txvName.setText(course.getName());
        }

        void clear() {
            txvId.setText("");
            txvName.setText("");
        }
    }

    private static DiffUtil.ItemCallback<Course> DIFF_CALLBACK = new DiffUtil.ItemCallback<Course>() {
        @Override
        public boolean areItemsTheSame(@NonNull Course oldItem, @NonNull Course newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Course oldItem, @NonNull Course newItem) {
            return oldItem.equals(newItem);
        }
    };
}
