package hamed.pagingroomlivedata;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Course.class}, version = 1)
public abstract class CourseDatabase extends RoomDatabase {
    abstract CourseDao courseDao();

    public static CourseDatabase INSTANSE = null;

    public static CourseDatabase getDataBase(Context context) {
        if (INSTANSE == null) {
            synchronized (CourseDatabase.class) {
                if (INSTANSE == null) {
                    INSTANSE = Room.databaseBuilder(context.getApplicationContext(),
                            CourseDatabase.class, "course.db")
                    .build();
                }
            }
        }

        return INSTANSE;
    }

}
