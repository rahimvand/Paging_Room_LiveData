package hamed.pagingroomlivedata;

import android.os.AsyncTask;


public class CourseRepository {

    void insertCourse(Course course,CourseDao dao){
        new InsertCourse(dao).execute(course);
    }

    private static class InsertCourse extends AsyncTask<Course,Void,Void>{
        private CourseDao dao;

        InsertCourse(CourseDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Course... courses) {
            dao.insert(courses[0]);
            return null;
        }
    }
}
