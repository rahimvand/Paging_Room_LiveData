package hamed.pagingroomlivedata;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

public class CourseViewModel extends AndroidViewModel {
    private CourseDao dao = CourseDatabase.getDataBase(getApplication()).courseDao();
    final LiveData<PagedList<Course>> courseList;
    private CourseRepository courseRepository = new CourseRepository();

    public CourseViewModel(@NonNull Application application) {
        super(application);

        //courseList = new LivePagedListBuilder<>(dao.getAllCourses(),50).build();
        PagedList.Config config = new PagedList.Config.Builder()
                .setPageSize(50)
                .setPrefetchDistance(150)
                .setEnablePlaceholders(true)
                .build();


        courseList = new LivePagedListBuilder<>(dao.getAllCourses(), config).build();
    }

    void insertCourse(Course course) {
        courseRepository.insertCourse(course, dao);
    }

}
