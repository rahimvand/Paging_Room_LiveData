package hamed.pagingroomlivedata;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rcvCourse;
    private CourseViewModel courseViewModel;
    private EditText edtCourse;
    private Button btnAddCourse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        courseViewModel = ViewModelProviders.of(this).get(CourseViewModel.class);
        initViews();
        generateAdapter();
        setListener();
    }

    private void initViews() {
        rcvCourse = findViewById(R.id.rcv_course);
        edtCourse = findViewById(R.id.edt_course);
        btnAddCourse = findViewById(R.id.btn_addCourse);
    }

    private void setListener() {
        btnAddCourse.setOnClickListener(v -> {
            courseViewModel.insertCourse(new Course(edtCourse.getText().toString()));
            Log.v("Course", "Course inserted :)");
        });
    }

    private void generateAdapter() {
        CourseAdapter courseAdapter = new CourseAdapter(this);
        rcvCourse.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rcvCourse.setAdapter(courseAdapter);

        //you can use this in java 8
        //courseViewModel.courseList.observe(this,courseAdapter::submitList);
        //for java 7
        courseViewModel.courseList.observe(this, new Observer<PagedList<Course>>() {
            @Override
            public void onChanged(PagedList<Course> courses) {
                Log.v("Course","#onChanged called & course list size is "+courses.size());
                courseAdapter.submitList(courses);
            }
        });

    }
}
